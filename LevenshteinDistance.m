function  d=LevenshteinDistance( s1, s2)

   
%      if (s1 == null || s2 == null)
%           throw new ArgumentNullException("s");
%      end
      L1 = length(s1);
      L2 = length(s2);

     if (L1 == 0)
           d=L2;end
     if (L2 == 0)
           d=L1;end

      distance =zeros(2, L2+1 );
      current = 2;

%      for  i = 1:L2
%           distance(1, i) = i;
%      end
distance(1,2:end) = 1:L2;
     for  i = 1:L1
     
           currentSwitch = 3 - current;
          distance(current, 1) = i;
          for j = 1:L2
          
                jMinus1 = j ;
               if (s1(i ) == s2(jMinus1))
                    distance(current, j+1) = distance(currentSwitch, jMinus1);
               else
               
                     a = distance(currentSwitch, jMinus1);
                     b = distance(currentSwitch, j+1);
                     c = distance(current, jMinus1);
                    if (a > b)
                         a = b;end
                    if (a > c)
                         a = c;end
                    distance(current, j+1) = a+ 1;
               
          
          
             end       
          end
          current = currentSwitch;
     end
     d=distance(3 - current, L2+1);
