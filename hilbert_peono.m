function II=hilbert_peono(I)
a=length(I);
N=log2(a)-1;
X=[1 2 2 1];
Y=[1 1 2 2];
for i=1:N
    Y1=2^i+1-Y;
    X1=2^i+1-X;
    X2=[Y  2^i+X 2^i+X Y1];
    Y2=[X Y 2^i+Y 2^i+X1];
    X=X2;Y=Y2;
end
for i=1:a*a
    II(i)=I(X(i),Y(i));
end